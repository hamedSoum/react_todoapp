function UserItem(props){
    console.log(props);
    return (
        <tr>
        <td>{props.id}</td>
        <td>{props.userName}</td>
        <td>{props.mail}</td>
        <td>{props.birthDate}</td>
        <td>{props.status === true ? 'enable' : "disable"}</td>
        <td>{props.password}</td>
        </tr>
    )
}
export default UserItem;
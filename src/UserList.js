import  './UserList.css'
import UserItem from './UserItem'
function UserList() {
    let userList = [
        {
            id: 1,
            userName : 'hamedSoum',
            mail : 'hamed@gmail.com',
            birthDate : '12/03/34',
            status : true,
            password : 'djfdjfd34455'
        },
        {
            id: 2,
            userName : 'soroDaouda',
            mail : 'soro@gmail.com',
            birthDate : '13/03/34',
            status : false,
            password : 'fjkffggk2343434'
        }
    ]
// const userList = userItems.map( user => <tr key={user.id}>
//     <td>{user.id}</td>
//     <td>{user.userName}</td>
//     <td>{user.mail}</td>
//     <td>{user.birthDate}</td>
//     <td>{user.status}</td>
//     <td>{user.password}</td>
// </tr>)
    return(
    <div className="wrapper">


         <h1 className="text-3xl font-bold ">User List!</h1>

         <div className='flex justify-end	'>
             <button className='bg-blue-600 py-2 px-4 mb-2 mx-2 rounded text-white'>add user</button>
         </div>
    <table className='table-fixed'>
       <thead>
           <tr>
           <th>id</th>
           <th>userName</th>
           <th>mail</th>
           <th>birthday</th>
           <th>status</th>
           <th>password</th>
           <th>Action</th>
           </tr>
       </thead>
       <tbody>
           {userList.map((user) => (
               <UserItem 
               key={user.id}
               id={user.id}
               userName={user.userName}
               mail={user.mail}
               birthDate={user.birthDate}
               status={user.status}
               password={user.password}
                 />
           ))}
       </tbody>
    </table>
</div>
    )
}

export default UserList; 